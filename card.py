'''
Jamey Barone
Final Project CIS177
July 5, 2016

This is a program to create a card.
It currently has one class for a standard card from a standard deck.
'''

from deck import StandardDeck

class StandardCard:
    
    # Initializer (constructor)
    def __init__(self, value):
        self.__value = value
        
    # Return the string representation of this card
    def __str__(self):
        #One is subtracted from the value because the 0 index of the deck is reserved
        #for the joker.
        suit = (self.__value - 1) // len(StandardDeck.ranks)  #integer from 0 to 3
        rank = (self.__value - 1) % len(StandardDeck.ranks)   #integer from 0 to 12
        string = StandardDeck.ranks[rank] + " of " + StandardDeck.suits[suit]
        return string

    # Return the integer value of this card - accessor method for __value
    def get_card_int(self):
        return self.__value

card = StandardCard(1)
print(card)


