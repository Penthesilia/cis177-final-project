#Final Project

This is a repository to track my final project card game (Fizbin Gaps)

Rules:
1. Number of cards dealt to each player depends on number of letters in (first?) name.

2. Players get a bonus of 2 points if their favorite color is blue.

3. Each turn the player may replace one card from their hand.  This goes on until the player 
with the most cards has had a chance to replace every card (ie number of rounds = longest player name)

4. Scoring:  1 point for every gap between cards. Total points wins.
   - This could be more intersting - thought about scoring gaps between 1 to 10 and runs from J to K?

Thoughts on programming flow:
- (DONE) Prompt user for name
  - Add some exception handling
  - There should probably be a max & min hand size

- (DONE) Create a list of computer user names so the computer can randomly generate a name.

- (DONE) Prompt user for favorite color.
  - (DONE) If "blue", add 2 points to score
  
- (DONE) Create and shuffle deck

- (DONE) Parse and count characters in players' names.
  - Stop at a space, in case player enters full name.
  
- (DONE) Deal cards based on name count.
  - The graphic for the computer hand needs to be changed to card backs.

- (DONE)Card swapping mechanic for human
   - Should the human be allowed to pass?  Probably

- (DONE) Card swapping for computer
  - Computer card swapping will be purely random for this program iteration.

- (DONE) Scoring mechanic
  - How to handle a tie??