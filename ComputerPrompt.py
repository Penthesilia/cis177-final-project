'''
Name: Jamey Barone (with code from mglison on Stackoverflow)
Created July 24, 2016
A popup allow the game to pause before the computer's turn
'''

#This class creates a simple "OK" popup and returns nothing

from tkinter import * # Import tkinter

class computerPrompt(object):
    def __init__(self,window):
        top=self.top=Toplevel(window)

        self.label=Label(top,text="Computer's Turn")
        self.label.pack()

        self.button=Button(top,text='Ok',command=self.cleanup)  #Button to end entry
        self.button.pack()

        self.top.attributes("-topmost", True)

    def cleanup(self):
        self.top.destroy()  #To remove the popup box


