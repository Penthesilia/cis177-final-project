'''
Name: Jamey Barone
Created July 22, 2016
A Popup to let the user select a discard
'''

from tkinter import * # Import tkinter
from deck import StandardCard

#This class creates a popup that asks for an input using radio buttons.
#It takes a "hand" of integers as an input, then converts the inputs to string
#representations of the cards and displays each one with a radio button.
#The player selects one card and that value is retured as "rbSelection"

class selectionPopup(object):
    def __init__(self,window,hand = []):  #One of the arguments brings in a hand of cards
        top=self.top=Toplevel(window)
        self.hand = hand

        self.label=Label(top,text="Choose a card to discard.")  #Label to prompt user
        self.label.pack()

        #Create the radio button variable to store the answer
        self.rbSelection = IntVar()

        #Create the list of radio buttons for the window and pack them
        self.rbList = []
        for i in range(len(self.hand)):
            self.card = StandardCard(self.hand[i]) #Use the standard card class to generate card name
            self.rbList.append(Radiobutton(top, text=str(self.card), variable = self.rbSelection, value = self.hand[i]))
            self.rbList[i].pack()

        self.button=Button(top,text='Ok',command=self.cleanup)  #Button to end entry
        self.button.pack()

        self.top.attributes("-topmost", True)

    def cleanup(self):
        self.value = self.rbSelection.get()  # Getter for the entry box
        self.top.destroy()  # To remove the popup box