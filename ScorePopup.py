'''
Name: Jamey Barone (with code from mglison on Stackoverflow)
Created July 26, 2016
A popup that shows the score at the end of the game
'''

#This class creates a popup that takes two scores as an input and displays them in a popup
#It returns nothing

from tkinter import * # Import tkinter

class scorePopup(object):
    def __init__(self,window,score1,name1, score2, name2):
        top=self.top=Toplevel(window)
        self.score1 = score1
        self.score2 = score2
        self.name1 = name1
        self.name2 = name2

        self.label1=Label(top,text="Score:")
        self.label1.pack()
        self.label2=Label(top,text=(name1 + " has a score of: " + str(score1) + " points"))
        self.label2.pack()
        self.label3=Label(top,text=(name2 + " has a score of: " + str(score2) + " points"))
        self.label3.pack()

        if (score1 > score2):
            self.label4 = Label(top, text=(name1 + " wins!"))
            self.label4.pack()
        elif (score2 > score1):
            self.label4 = Label(top, text=(name2 + " wins!"))
            self.label4.pack()
        else:
            self.label4 = Label(top, text="It's a tie!")
            self.label4.pack()

        self.button=Button(top,text='Ok',command=self.cleanup)  #Button to end entry
        self.button.pack()

        self.top.attributes("-topmost", True)

    def cleanup(self):
        self.top.destroy()  #To remove the popup box


