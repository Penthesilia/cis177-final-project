'''
Jamey Barone
Final Project CIS177
Created July 5, 2016
Modified July 19, 2016

This is a program to create decks.
Currently it contains only one class called StandardDeck
'''

import random
from tkinter import *  # Import tkinter

class StandardDeck:

    #This class creates a standard deck as a list of integers and also
    #contains the lists of suits and ranks (as made in lab)

    suits = ["Spades", "Hearts", "Diamonds", "Clubs"]
    ranks = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"]
    size = len(suits) * len(ranks)

    # Initializer (constructor)
    def __init__(self):

        self.deck = []
        for i in range (self.size + 1):  #Creates a "deck" of integers, one more than the size
            self.deck.append(i)          #The 0 index represents the joker

    def shuffle(self):
        random.shuffle(self.deck)

class StandardCard:

    #This class creates a string result for a single card when a single
    #integer value is passed to it (similar to the one made in lab)

    # Initializer (constructor)
    def __init__(self, value):
        self.__value = value

    # Return the string representation of this card
    def __str__(self):
        if self.__value == 0:
            string = "Joker"
            return string

        # One is subtracted from the value because the 0 index of the deck is reserved
        # for the joker.
        suit = (self.__value - 1) // len(StandardDeck.ranks)  # integer from 0 to 3
        rank = (self.__value - 1) % len(StandardDeck.ranks)  # integer from 0 to 12
        string = StandardDeck.ranks[rank] + " of " + StandardDeck.suits[suit]
        return string

    # Return the integer value of this card - accessor method for __value
    def get_card_int(self):
        return self.__value

from tkinter import *  # Import tkinter

class CardGUI():

    #This class returns a fram pakced the image of a single card when a single
    #integer value is passed to it.

    def __init__(self, root, cardValue):
        self.root = root
        self.cardValue = cardValue

        #Create a single image and put it into a label
        self.cardImage = PhotoImage(file = "cards/" + str(self.cardValue) + ".gif")
        self.label = Label(self.root, image = self.cardImage)
        self.label.grid(row=0, column=0)

class CardListGUI():

    #This class returns a frame packed with a list of images for a "hand" of cards when a
    #list of integers is passed to it.

    def __init__(self, root, handSize = []):
        self.root = root
        self.handSize = handSize

        #The code to create a list of images, as per lab
        self.imageList = []
        for i in handSize:
            self.imageList.append(PhotoImage(file="cards/" + str(i) + ".gif"))

        #The code to put the images into a list of labels, as per lab
        self.labelList = []
        for i in range(len(self.imageList)):
            self.labelList.append(Label(self.root, image=self.imageList[i]))
            self.labelList[i].pack(side=LEFT)

class CardBackGUI():

    #This class returns a frame packed with multiple images of a card back.
    #when a list of integers is passed to it.

    def __init__(self, root, handSize = []):
        self.root = root
        self.handSize = handSize

        #The code to create a list of images, as per lab
        self.imageList = []
        for i in handSize:
            self.imageList.append(PhotoImage(file="cards/b1fv.gif"))

        #The code to put the images into a list of labels, as per lab
        self.labelList = []
        for i in range(len(self.imageList)):
            self.labelList.append(Label(self.root, image=self.imageList[i]))
            self.labelList[i].pack(side=LEFT)






