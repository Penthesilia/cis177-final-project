'''
Name: Jamey Barone (with code from mglison on Stackoverflow)
Created July 21, 2016
A Popup to ask questions in the game
'''

#This class creates a popup that asks a question.
#It takes the end of the quesion as an input.
#It returns the answer as a reference variable

from tkinter import * # Import tkinter

class questionPopup(object):
    def __init__(self,window,question):  #One of the arguments finishes the question
        top=self.top=Toplevel(window)

        self.label=Label(top,text="What is "+ question +"?")  #Label to ask the question
        self.label.pack()

        self.entry=Entry(top)  #Entry box to receive an answer
        self.entry.pack()

        self.button=Button(top,text='Ok',command=self.cleanup)  #Button to end entry
        self.button.pack()

        self.top.attributes("-topmost", True)

        #Sound bite credits
        self.labelMP=Label(top, text="Credit Monty Python & The Holy Grail for sound bite", font=("Helvetica",8))
        self.labelMP.pack()

    def cleanup(self):
        self.value=self.entry.get() #Getter for the entry box
        self.top.destroy()  #To remove the popup box


