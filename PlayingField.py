'''
Name: Jamey Barone
Created July 19, 2016
Modified July 22, 2016
Playing field for card game project
'''

from tkinter import *  # Import tkinter
import random
from winsound import *
from deck import *  # Import classes for the deck of cards
from Popup import *
from selectionPopup import *
from ComputerPrompt import *
from ScorePopup import *

class PlayingField:
    def __init__(self):
        root = Tk() #Create a window
        self.root = root
        root.title("Fizzbin Gap Game")

        self.namePopup()  # This calls the function to get the player's name
        self.colourPopup()  # This calls the function to get the player's favorite color.
        self.getComputerName()  # This calls the function to generate the computer's name

        self.gameDeck = StandardDeck()  # Create a standard deck of cards for the GUI
        self.gameDeck.shuffle()  # Shuffle the deck


        # Generate the human player's hand based on the numbers of letters in their name
        self.humanHand = []
        for i in range (len(self.getNameValue())):
            self.humanHand.append (self.gameDeck.deck.pop(i))#Generate the player's hand


        # Generate the computer's hand based on the numbers of letters in their name
        self.compHand = []
        for i in range (len(self.compName)):
            self.compHand.append (self.gameDeck.deck.pop(i))


        # Create PhotoImage Objects
        ComputerImage = PhotoImage(file = "images/Computer.gif")
        PlayerImage = PhotoImage (file = "images/meeple.gif")
        DeckImage = PhotoImage(file = "cards/b1fv.gif")
        DiscardImage = PhotoImage (file="cards/Discard.gif")

        # Create Field Elements
        labelComputer = Label(root, image = ComputerImage)
        self.frameCompCards = Frame(root)
        labelCompName = Label(root, text = self.compName)
        labelHuman = Label(root, image = PlayerImage)
        self.frameHumanCards = Frame(root)
        labelHumanName = Label(root, text = str(self.getNameValue()))
        labelDeck = Label(root, image = DeckImage)
        self.labelDiscards = Label(root, image = DiscardImage)

        # Place elements in grid
        self.frameCompCards.grid(row=1, column=2, columnspan=4, sticky = S)

        #This line will need to be changed to show card backs:
        CompCardList = CardBackGUI(self.frameCompCards, self.compHand)

        labelComputer.grid(row=1, column=6)
        labelCompName.grid(row=2, column=6)
        labelDeck.grid(row=3, column=3, padx = 10)
        self.labelDiscards.grid(row=3, column=4, padx = 10)
        labelHumanName.grid(row=4, column=1)
        labelHuman.grid(row=5, column=1)
        self.frameHumanCards.grid(row=5, column=2, columnspan=4, sticky = N)
        self.HumanCardList = CardListGUI(self.frameHumanCards, self.humanHand)


        # Game mechanic:

        #Determine the number of turns to equal the larger hand size
        if (len(self.compHand) <= len(self.humanHand)):
            numberOfTurns = len(self.compHand)
        else:
            numberOfTurns = len(self.humanHand)

        for i in range (0,numberOfTurns):
            self.cardSelect()  #Ask player to choose a discard
            self.humanDiscards()  #Remove discard from hand and show in discar pile
            self.humanHand.append(self.gameDeck.deck.pop(0))  # Add a new card to human hand
            self.redrawHumanHand()  #The command to show the new player hand
            self.compTurnOK()
            self.computerDiscards()  #This calls the function for the computer to discard
            self.compHand.append(self.gameDeck.deck.pop(0))  # Add a new card to computer hand
            self.redrawComputerand()  #Remove this command at final game!!
        self.scores = self.Score(self.humanHand, self.compHand)
        scorePopup(root,self.scores[0],str(self.getNameValue()),self.scores[1],self.compName)


        root.mainloop()

    # Functions to ask the human to select a card for discard
    def cardSelect(self):
        self.cardSelection = selectionPopup(self.root, self.humanHand)
        self.root.wait_window(self.cardSelection.top)
    def getSelectionValue(self):
        return self.cardSelection.value

    # Function to have the human discard a card and put it in the discard pile.
    def humanDiscards(self):
        j = 0
        while j < len(self.humanHand):
            if self.humanHand[j] == self.getSelectionValue():
                self.humanHand.pop(j)
                self.NewDiscardImage = PhotoImage(file="cards/" + str(self.getSelectionValue()) + ".gif")
            j += 1
        self.labelDiscards["image"] = self.NewDiscardImage


    # A function to erase the previous player hand and show the new one
    def redrawHumanHand(self):
        self.frameHumanCards.destroy()  #Destroy the frame that shows the human's hand
        self.frameHumanCards = Frame(self.root)  #Rebuild the frame
        self.frameHumanCards.grid(row=5, column=2, columnspan=4, sticky=N)  #Place the new frame in the grid
        self.HumanCardList = CardListGUI(self.frameHumanCards, self.humanHand)  #Get the image for the new hand and put it in the frame

    # Function to have the computer discard a random card and put it in the discard pile.
    def computerDiscards(self):
        computerDiscard = random.randint (0,len(self.compName)-1) #Generate a random number to represent the index of computer hand's discard
        self.NewDiscardImage = PhotoImage(file="cards/" + str(self.compHand[computerDiscard]) + ".gif")
        self.compHand.pop(computerDiscard)
        self.labelDiscards["image"] = self.NewDiscardImage

    #This function is only necessary in the testing phase when the computer hand is visible
    # A function to erase the previous computer hand and show the new one
    def redrawComputerand(self):
        self.frameCompCards.destroy()  #Destroy the frame that shows the computer's hand
        self.frameCompCards = Frame(self.root)  #Rebuild the frame
        self.frameCompCards.grid(row=1, column=2, columnspan=4, sticky = S)  #Place the new frame in the grid
        self.CompCardList = CardListGUI(self.frameCompCards, self.compHand)  #Get the image for the new hand and put it in the frame

    #Function to prompt OK for computer turn.  Mostly just to pause between turns.
    def compTurnOK(self):
        self.compTurn = computerPrompt(self.root)
        self.root.wait_window(self.compTurn.top)

    # Functions to as ask the human for their name and save the answer
    def namePopup(self):
        self.humanName = questionPopup(self.root,"your name")
        PlaySound('sounds/name.wav', SND_FILENAME)
        self.root.wait_window(self.humanName.top)
    def getNameValue(self):
        return self.humanName.value

    # Functions to ask the human for their favorite color and save the answer
    def colourPopup(self):
        self.faveColor = questionPopup(self.root," your favorite color")
        PlaySound('sounds/colour.wav', SND_FILENAME)
        self.root.wait_window(self.faveColor.top)
    def getfaveColor(self):
        return self.faveColor.value.lower()

    # Function to generate a computer name from a predefined list of names
    def getComputerName(self):
        self.compNames = ["Tim", "Zoot", "Brian", "Arthur", "Galahad", "Dinsdale", "Mrs. Scum"]
        random.shuffle(self.compNames)
        self.compName = self.compNames[0]

    #Count the score
    def Score(self,humanHand=[],compHand=[]):
        self.humanHand = humanHand
        self.compHand = compHand
        self.humanScore = 0
        self.compScore = 0

        #Bonus two points if favorite color is blue
        if "blue" in self.getfaveColor():
            self.humanScore += 2


        #Convert the two hands to values from 0 to 12 (will contain duplicates)
        #The formula takes the numerical value and subtracts 1 because the 0 value is the joker.
        #And since the joker is the zero card, the modulus formula below will make it worth -1
        #which is a bonus point in the scoring

        for i in range(0, len(self.humanHand)):
            self.humanHand[i] = (self.humanHand[i]-1) % 13
        for i in range(0, len(self.compHand)):
            self.compHand[i] = (self.compHand[i]-1) % 13

        self.humanSet = set(self.humanHand) #Turn the hand into a set of values to eliminate duplicate values
        self.compSet = set(self.compHand)  #Turn the hand into a set of values to eliminate duplicate values

        self.humanHand = list(self.humanSet) #Turn the hand back into a list for indexing
        self.compHand = list (self.compSet)  #Turn the hand back into a list for indexing

        self.humanHand.sort() #Sort the list for ease of calculation
        self.compHand.sort()  #Sort the list for east of calculation

        for i in range (0, (len(self.humanHand)-1)):
            self.humanScore += self.humanHand[i+1] - self.humanHand[i]

        for i in range (0, (len(self.compHand)-1)):
            self.compScore += self.compHand[i+1] - self.compHand[i]

        return (self.humanScore, self.compScore)


PlayingField()






