'''
Name: Jamey Barone
Date: July 18, 2016
Deck of Cards Simulator in Tkinter
'''

from tkinter import * # Import tkinter
import random

SIZE_OF_HAND = 4

class DeckOfCardsGUI:
    def __init__(self):
        window = Tk()
        window.title("Pick A Hand of Random Cards")

        #Creates a list of image gifs
        self.imageList = []
        for i in range (1, 53):
            self.imageList.append(PhotoImage(file = "cards/" + str(i) + ".gif"))

        frame = Frame(window)  #Create a frame to display the card images
        frame.pack()

        #Create a list of labels, which are used to display graphics in Tkinter
        self.labelList = []
        for i in range(52):
            self.labelList.append(Label(frame, image = self.imageList[i]))
            self.labelList[i].pack(side = LEFT)

        Button(window, text="Shuffle", command = self.shuffle).pack()

        window.mainloop()

    def shuffle(self):
        random.shuffle(self.imageList)
        for i in range(52):
            self.labelList[i]["image"] = self.imageList[i]


def main():
    deck = DeckOfCardsGUI()
    print (deck.imageList[1])

main()
